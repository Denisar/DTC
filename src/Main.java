import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Scanner;
import java.awt.Frame;

public class Main extends Frame{

    public static void main(String[] args) {

        JFrame f = new JFrame("DTC");
        f.setSize(500,200);
        f.setLocation(100,100);



        JFormattedTextField txtSize = new JFormattedTextField(0.0f);
        txtSize.setBounds(25,50,200,25);

        JLabel lblSize = new JLabel("Download size in GB");
        lblSize.setBounds(25,25,200,25);

        JFormattedTextField txtSpeed = new JFormattedTextField(0.0f);
        txtSpeed.setBounds(250,50,200,25);

        JLabel lblSpeed = new JLabel("Download speed in Mb/s");
        lblSpeed.setBounds(250,25,200,25);

        JLabel lblDownloadSpeed = new JLabel("0hrs 0min 0s");
        lblDownloadSpeed.setBounds(200,100,200,25);

        Action accept = new AbstractAction("GO") {
            @Override
            public void actionPerformed(ActionEvent e) {
                String txt = CalculateSpeed(Float.parseFloat(txtSize.getValue().toString()),Float.parseFloat(txtSpeed.getValue().toString()));
                lblDownloadSpeed.setText(txt);
            }
        };

        JButton btnGo = new JButton(accept);
        f.getRootPane().setDefaultButton(btnGo);

        btnGo.setBounds(450,50,50,25);


        f.add(lblSize);
        f.add(txtSize);
        f.add(lblSpeed);
        f.add(txtSpeed);
        f.add(lblDownloadSpeed);
        f.add(btnGo);

        f.setLayout(null);

        f.setVisible(true);

    }

    public static String CalculateSpeed(float size , float speed){
        size = (size * 1000) * 8;//Convert to gigabit
        float time = size / speed;
        int seconds = (int)time % 60;
        int minutes = (int)(time / 60);
        minutes = minutes % 60;
        int hours = (int)time/3600;

        return hours+"hrs "+minutes+"min " + seconds+"s";
    }


}
